package com.pscxtc.shop.service;

import com.pscxtc.common.R;

import java.util.Map;

/**
 * TODO: 类注释
 *
 * @author chenxu
 * @since 2021年04月06日
 */
public interface IShopService {
    public R create(Map<String, Object> params);
}
