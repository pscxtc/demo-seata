package com.pscxtc.shop.service.impl;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.pscxtc.client.IAccountClient;
import com.pscxtc.client.IGoodsClient;
import com.pscxtc.client.IOrdersClient;
import com.pscxtc.client.IStockClient;
import com.pscxtc.common.R;
import com.pscxtc.domain.Account;
import com.pscxtc.domain.Goods;
import com.pscxtc.domain.Orders;
import com.pscxtc.domain.Stock;
import com.pscxtc.exception.BusinessException;
import com.pscxtc.shop.service.IShopService;
import io.seata.spring.annotation.GlobalTransactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Map;
import java.util.Optional;

/**
 * TODO: 类注释
 *
 * @author chenxu
 * @since 2021年04月06日
 */
@Service
@Slf4j
public class ShopServiceImpl implements IShopService {

    @Autowired
    private IOrdersClient ordersClient;
    @Autowired
    private IGoodsClient goodsClient;
    @Autowired
    private IAccountClient accountClient;
    @Autowired
    private IStockClient stockClient;

    @GlobalTransactional
    @Transactional
    @Override
    public R create(Map<String, Object> params) {

        JSONObject jsonObject = JSONUtil.parseObj(params);
        log.info("jsonObject:{}",jsonObject.toString());

        Date now = new Date();

        //确认账号
        Optional<Account> accountOptional = accountClient.findOne(new Account(null,jsonObject.getStr("userId"),null));
        if (!accountOptional.isPresent()){
            log.warn("account 为空/不存在");
            throw new BusinessException("account 为空/不存在");
        }
        //确认商品
        Optional<Goods> goodsOptional = goodsClient.findById(jsonObject.getStr("goodsId"));
        if ( !goodsOptional.isPresent() || null == goodsOptional.get().getId()){
            log.warn("goods 为空/不存在");
            throw new BusinessException("goods 为空/不存在");
        }
        //确认库存
        Optional<Stock> stockOptional = stockClient.findOne(new Stock(null,jsonObject.getStr("goodsId"),null,null));
        if (!stockOptional.isPresent() || stockOptional.get().getGoodsStock() < 1){
            log.warn("stock 为空/ 小于1");
            throw new BusinessException("stock 为空/ 小于1");
        }

        Long totalCost = goodsOptional.get().getCost()*jsonObject.getLong("goodsCount");

        //创建订单
        Orders orders = ordersClient.create( new Orders(null,accountOptional.get().getId(),goodsOptional.get().getId(),jsonObject.getLong("goodsCount"),totalCost ,now));
        if (null == orders){
            log.warn("orders 创建失败");
            throw new BusinessException("orders 创建失败");
        }

        //扣除库存
        if (!stockClient.updateStock(goodsOptional.get().getId(),jsonObject.getLong("goodsCount")).isResultFlag()){
            log.warn("扣除库存失败");
            throw new BusinessException("扣除库存失败");
        };

        //扣除金额
        if (!accountClient.updateAccount(accountOptional.get().getId(),totalCost).isResultFlag()){
            log.warn("扣除金额失败");
            throw new BusinessException("扣除金额失败");
        }
        return R.ok("操作成功");
    }
}
