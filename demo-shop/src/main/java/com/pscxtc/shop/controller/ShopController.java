package com.pscxtc.shop.controller;

import cn.hutool.core.collection.CollUtil;
import com.pscxtc.client.IShopClient;
import com.pscxtc.common.R;
import com.pscxtc.shop.service.IShopService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * 下单总入口
 *
 * @author chenxu
 * @since 2021年04月06日
 */
@RequestMapping("/shop")
@RestController
@Slf4j
@Api(value="下单controller",tags = {"下单controller"})
public class ShopController implements IShopClient {

    @Autowired
    private IShopService shopService;

    @ApiOperation(value = "下单接口")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId",value = "用户id",required = true,dataTypeClass = String.class),
            @ApiImplicitParam(name = "goodsId",value = "货物id",required = true,dataTypeClass = String.class),
            @ApiImplicitParam(name = "goodsCount",value = "下单货物数量",required = true,dataTypeClass = Long.class),
    })
    @Override
    public R create(String userId, String goodsId, Long goodsCount) {
        Map<String,Object> params = CollUtil.newHashMap();
        params.put("userId",userId);
        params.put("goodsId",goodsId);
        params.put("goodsCount",goodsCount);
        return shopService.create(params);
    }
}
