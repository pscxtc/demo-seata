//package com.pscxtc.stock.service.impl;
//
//import com.pscxtc.common.R;
//import com.pscxtc.domain.Stock;
//import com.pscxtc.exception.BusinessException;
//import com.pscxtc.stock.repository.IStockRepository;
//import com.pscxtc.stock.service.IStockService;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.domain.Example;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import java.util.Date;
//import java.util.Optional;
//
///**
// * TODO: 类注释
// *
// * @author chenxu
// * @since 2021年04月06日
// */
//@Slf4j
//@Service
//public class StockServiceImpl implements IStockService {
//
//    @Autowired
//    private IStockRepository stockRepository;
//
//    @Transactional
//    @Override
//    public R updateStock(String goodsId, Long goodsNum) {
//        Optional<Stock> stockOptional = stockRepository.findOne(Example.of(new Stock(null,goodsId,null,null)));
//        if (!stockOptional.isPresent()){
//            log.warn("stock 获取失败,goodsId:{}",goodsId);
//            throw new BusinessException("stock 获取失败");
//        }
//        stockOptional.get().setGoodsStock(stockOptional.get().getGoodsStock()-goodsNum);
//        if (stockOptional.get().getGoodsStock() < 0){
//            log.warn("stock 库存不足,goodsId:{},goodsNum:{}",goodsId,goodsNum);
//            throw new BusinessException("stock 库存不足");
//        }
//        stockOptional.get().setUpdateTime(new Date());
//        stockRepository.save(stockOptional.get());
//        log.info("stock 保存成功");
//        return R.ok();
//    }
//}
