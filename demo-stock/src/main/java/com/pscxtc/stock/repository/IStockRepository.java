package com.pscxtc.stock.repository;

import com.pscxtc.domain.Stock;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

/**
 * TODO: 类注释
 *
 * @author chenxu
 * @since 2021年04月06日
 */
@Service
public interface IStockRepository extends JpaRepository<Stock,String> {
}
