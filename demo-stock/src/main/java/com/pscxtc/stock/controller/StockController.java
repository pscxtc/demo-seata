package com.pscxtc.stock.controller;

import com.pscxtc.client.IStockClient;
import com.pscxtc.common.R;
import com.pscxtc.domain.Stock;
import com.pscxtc.exception.BusinessException;
import com.pscxtc.stock.repository.IStockRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.Optional;

/**
 * TODO: 类注释
 *
 * @author chenxu
 * @since 2021年04月06日
 */
@RequestMapping("/stock")
@RestController
@Slf4j
public class StockController implements IStockClient {

    @Autowired
    private IStockRepository stockRepository;
//    @Autowired
//    private IStockService stockService;

    @Transactional
    @Override
    public R updateStock(String goodsId, Long goodsNum) {
//        return stockService.updateStock(goodsId,goodsNum);
        Optional<Stock> stockOptional = stockRepository.findOne(Example.of(new Stock(null,goodsId,null,null)));
        if (!stockOptional.isPresent()){
            log.warn("stock 获取失败,goodsId:{}",goodsId);
            throw new BusinessException("stock 获取失败");
        }
        stockOptional.get().setGoodsStock(stockOptional.get().getGoodsStock()-goodsNum);
        if (stockOptional.get().getGoodsStock() < 0){
            log.warn("stock 库存不足,goodsId:{},goodsNum:{}",goodsId,goodsNum);
            throw new BusinessException("stock 库存不足");
        }
        stockOptional.get().setUpdateTime(new Date());
        stockRepository.save(stockOptional.get());
        log.info("stock 保存成功");
        return R.ok();
    }

    @Override
    public Optional<Stock> findOne(Stock stock) {
        return stockRepository.findOne(Example.of(stock));
    }
}
