package com.pscxtc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class DemoGoodsApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoGoodsApplication.class, args);
    }

}
