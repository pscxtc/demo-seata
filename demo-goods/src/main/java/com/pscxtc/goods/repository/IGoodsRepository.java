package com.pscxtc.goods.repository;

import com.pscxtc.domain.Goods;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

/**
 * TODO: 类注释
 *
 * @author chenxu
 * @since 2021年04月06日
 */
@Service
public interface IGoodsRepository extends JpaRepository<Goods,String> {
}
