package com.pscxtc.goods.controller;

import com.pscxtc.client.IGoodsClient;
import com.pscxtc.domain.Goods;
import com.pscxtc.goods.repository.IGoodsRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

/**
 * TODO: 类注释
 *
 * @author chenxu
 * @since 2021年04月06日
 */
@RequestMapping("/goods")
@RestController
@Slf4j
public class GoodsController implements IGoodsClient {

    @Autowired
    private IGoodsRepository goodsRepository;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Optional<Goods> findById(String goodsId) {
        return goodsRepository.findById(goodsId);
    }
}
