package com.pscxtc.orders.service.impl;

import com.pscxtc.domain.Orders;
import com.pscxtc.orders.repository.IOrdersRepository;
import com.pscxtc.orders.service.IOrdersService;
import com.pscxtc.snowflake.IdUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * TODO: 类注释
 *
 * @author chenxu
 * @since 2021年04月06日
 */
@Service
public class OrdersServiceImpl implements IOrdersService {

    @Autowired
    private IOrdersRepository ordersRepository;

    @Transactional
    @Override
    public Orders create(Orders orders) {
        orders.setId(IdUtil.createId());
        return ordersRepository.save(orders);
    }
}
