package com.pscxtc.orders.repository;

import com.pscxtc.domain.Orders;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

/**
 * TODO: 类注释
 *
 * @author chenxu
 * @since 2021年04月06日
 */
@Service
public interface IOrdersRepository extends JpaRepository<Orders,String> {
}
