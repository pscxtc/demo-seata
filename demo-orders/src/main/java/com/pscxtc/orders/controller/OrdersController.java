package com.pscxtc.orders.controller;

import com.pscxtc.client.IOrdersClient;
import com.pscxtc.domain.Orders;
import com.pscxtc.orders.service.IOrdersService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * TODO: 类注释
 *
 * @author chenxu
 * @since 2021年04月06日
 */
@RequestMapping("/orders")
@RestController
@Slf4j
public class OrdersController implements IOrdersClient {

    @Autowired
    private IOrdersService ordersService;

    @Override
    public Orders create(Orders orders) {
        return ordersService.create(orders);
    }
}
