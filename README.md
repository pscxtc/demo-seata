# demo-seata

#### 介绍
seata1.4.1+nacos1.3.1 练习,官网示例太陈旧,配合文档,有全套的说明

#### 软件架构
+ jpa+springcloud+seata1.4.1+nacos1.3.1
+ seata 采用mysql存储,后续利于集群,事务处理方式 AT
+ 明确api-service-business分层结构,api负责定义接口,service负责独立逻辑处理,business负责多service合并逻辑处理,这样避免了service之间相互引用
+ 舍弃service层,简化结构(不影响事务)

#### 安装教程

1. 自行配置及安装nacos
2. 根据 [seata 安装及配置](https://www.yuque.com/docs/share/eaa31137-00a7-4528-85da-d9fbbe8a1ef0) 进行安装及配置)
3. 修改各项目 bootstrap.yml 内的 nacos地址及数据库地址
4. 采用 doc/demo_seata.sql 建立数据库,表及内容, 启动 account,goods,orders,stock,shop 项目,所有操作入库在 shop的controller下

#### 使用说明

1.  [knife4j接口地址](http://127.0.0.1:16084/doc.html)
2.  选择下单接口->调试,输入如下参数:  goodsCount:100,goodsId:100,userId:1 会提示异常信息,查看数据库 回滚成功
3.  上方接口,goodsCount改为1,下单成功,扣除正常

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request