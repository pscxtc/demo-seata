//package com.pscxtc.account.service.impl;
//
//import com.pscxtc.account.repository.IAccountRepository;
//import com.pscxtc.account.service.IAccountService;
//import com.pscxtc.common.R;
//import com.pscxtc.exception.BusinessException;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
///**
// * TODO: 类注释
// *
// * @author chenxu
// * @since 2021年04月06日
// */
//@Service
//@Slf4j
//public class AccountServiceImpl implements IAccountService {
//
//    @Autowired
//    private IAccountRepository accountRepository;
//
//    @Transactional
//    @Override
//    public R updateAccount(String accoutId, Long cost) {
//        int result = accountRepository.updateAccountMoney(accoutId,cost);
//        if ( result < 1){
//            log.warn("更新账户失败,金额/账号异常");
//            throw new BusinessException("金额/账号异常");
//        }
//        log.info("更新账户完毕");
//        return R.ok();
//    }
//}
