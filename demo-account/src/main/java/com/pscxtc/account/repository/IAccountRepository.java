package com.pscxtc.account.repository;

import com.pscxtc.domain.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

/**
 * Account
 *
 * @author chenxu
 * @since 2021年04月02日
 */
public interface IAccountRepository extends JpaRepository<Account,String> {

    @Modifying
    @Query(value = "update Account set money=(money-?2) where id = ?1 and money >= ?2 ")
    public int updateAccountMoney(String accoutId, Long cost);
}
