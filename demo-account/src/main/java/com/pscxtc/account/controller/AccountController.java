package com.pscxtc.account.controller;

import com.pscxtc.account.repository.IAccountRepository;
import com.pscxtc.client.IAccountClient;
import com.pscxtc.common.R;
import com.pscxtc.domain.Account;
import com.pscxtc.exception.BusinessException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

/**
 * Account 对外接口
 *
 * @author chenxu
 * @since 2021年04月02日
 */
@RequestMapping("/account")
@RestController
@Slf4j
public class AccountController implements IAccountClient {

    @Autowired
    private IAccountRepository accountRepository;
//    @Autowired
//    private IAccountService accountService;

    @Override
    public String testA(String a){
        return "你好:"+a;
    }

    @RequestMapping("/testR")
    public R testR(String a){
        R r = R.ok();
        r.setData(a);
        return r;
    }

    @Transactional
    @Override
    public R updateAccount(String accoutId, Long cost) {
//        return accountService.updateAccount(accoutId,cost);
        int result = accountRepository.updateAccountMoney(accoutId,cost);
        if ( result < 1){
            log.warn("更新账户失败,金额/账号异常");
            throw new BusinessException("金额/账号异常");
        }
        log.info("更新账户完毕");
        return R.ok();
    }

    @Override
    public Optional<Account> findOne(Account account) {
        return accountRepository.findOne(Example.of(account));
    }

}
