package com.pscxtc.common;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 通用返回消息类
 *
 * @author chenxu
 * @since 2021年04月08日
 */
@Data
@NoArgsConstructor
public class R {
    private boolean resultFlag ;
    private String message ;
    private String data;

    public static R ok(String message,String data){
        R r = new R();
        r.setResultFlag(true);
        r.setMessage(message);
        r.setData(data);
        return r;
    }

    public static R ok(String message){
        return R.ok(message,null);
    }

    public static R ok(){
        return R.ok(null,null);
    }

    public static R error(String message,String data){
        R r = new R();
        r.setResultFlag(false);
        r.setMessage(message);
        r.setData(data);
        return r;
    }

    public static R error(String message){
        return R.error(message,null);
    }

}
