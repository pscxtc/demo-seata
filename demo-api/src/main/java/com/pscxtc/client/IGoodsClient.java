package com.pscxtc.client;

import com.pscxtc.api.IGoodsCommon;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 商品
 *
 * @author chenxu
 * @since 2021年04月02日
 */
@FeignClient(name = "demo-goods",path = "/goods")
public interface IGoodsClient extends IGoodsCommon {
}
