package com.pscxtc.client;

import com.pscxtc.api.IAccountCommon;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 账号
 *
 * @author chenxu
 * @since 2021年04月02日
 */
@FeignClient(name = "demo-account",path = "/account")
public interface IAccountClient extends IAccountCommon {
}
