package com.pscxtc.client;

import com.pscxtc.api.IShopCommon;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 商品
 *
 * @author chenxu
 * @since 2021年04月02日
 */
@FeignClient(name = "demo-shop",path = "/shop")
public interface IShopClient extends IShopCommon {
}
