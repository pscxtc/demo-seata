package com.pscxtc.client;

import com.pscxtc.api.IStockCommon;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 商品
 *
 * @author chenxu
 * @since 2021年04月02日
 */
@FeignClient(name = "demo-stock",path = "/stock")
public interface IStockClient extends IStockCommon {
}
