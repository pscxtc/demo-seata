package com.pscxtc.client;

import com.pscxtc.api.IOrdersCommon;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * 订单
 *
 * @author chenxu
 * @since 2021年04月02日
 */
@FeignClient(name = "demo-orders",path = "/orders")
public interface IOrdersClient extends IOrdersCommon {
}
