package com.pscxtc.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

/**
 * 订单类
 *
 * @author chenxu
 * @since 2021年04月06日
 */
@Entity
@Table(indexes = {
        @Index(columnList = "accountId,goodsId")
})
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Orders {
    @Id
    @Column(length = 50)
    private String id;
    @Column(length = 50)
    private String accountId;
    @Column(length = 50)
    private String goodsId;
    private Long goodsCount;
    private Long totalCost;
    private Date creatTime;

}
