package com.pscxtc.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * 账户类
 *
 * @author chenxu
 * @since 2021年04月02日
 */
@Entity
@Table(uniqueConstraints = {
        @UniqueConstraint(columnNames = {"userId"})
})
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Account {
    @Id
    @Column(length = 50)
    private String id;
    @Column(length = 50)
    private String userId;
    private Long money;
}
