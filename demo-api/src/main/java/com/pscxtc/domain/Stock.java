package com.pscxtc.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

/**
 * 库房类
 *
 * @author chenxu
 * @since 2021年04月06日
 */
@Entity
@Table(uniqueConstraints = {
        @UniqueConstraint(columnNames = {"goodsId"})
})
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Stock {
    @Id
    @Column(length = 50)
    private String id;
    @Column(length = 50)
    private String goodsId;
    private Long goodsStock;
    private Date updateTime;

}
