package com.pscxtc.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * 商品类
 *
 * @author chenxu
 * @since 2021年04月02日
 */
@Entity
@Table(uniqueConstraints = {
        @UniqueConstraint(columnNames = {"name"})
})
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Goods {
    @Id
    @Column(length = 50)
    private String id;
    private String name;
    private Long cost;
}
