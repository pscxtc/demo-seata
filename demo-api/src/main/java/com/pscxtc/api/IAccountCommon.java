package com.pscxtc.api;

import com.pscxtc.common.R;
import com.pscxtc.domain.Account;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Optional;

/**
 * Account 通用
 *
 * @author chenxu
 * @since 2021年04月02日
 */
public interface IAccountCommon {

    @RequestMapping("/testA")
    public String testA(String a);

    @RequestMapping(value = "/updateAccount", method= RequestMethod.GET)
    public R updateAccount(@RequestParam("accoutId") String accoutId, @RequestParam("cost") Long cost);

    @RequestMapping(value = "/findOne",method = RequestMethod.POST)
    public Optional<Account> findOne(@RequestBody Account account);
}
