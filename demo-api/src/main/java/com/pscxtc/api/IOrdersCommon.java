package com.pscxtc.api;

import com.pscxtc.domain.Orders;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Orders 通用
 *
 * @author chenxu
 * @since 2021年04月02日
 */
public interface IOrdersCommon {

    /**
     * 创建订单
     *
     * @param orders    订单信息
     *
     * @return {@link boolean}
     * @since 2021/4/6
     * 版本历史:
     * Date         Author         Description
     *---------------------------------------------------------*
     * 2021/4/6   chenxu          初始创建
     */
    @RequestMapping(value = "/create",method = RequestMethod.POST)
    public Orders create(@RequestBody Orders orders);
}
