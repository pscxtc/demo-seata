package com.pscxtc.api;

import com.pscxtc.domain.Goods;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Optional;

/**
 * Account 通用
 *
 * @author chenxu
 * @since 2021年04月02日
 */
public interface IGoodsCommon {

    @RequestMapping(value = "/findById", method= RequestMethod.GET)
    public Optional<Goods> findById(@RequestParam("goodsId") String goodsId);
}
