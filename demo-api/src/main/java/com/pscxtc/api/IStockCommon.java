package com.pscxtc.api;

import com.pscxtc.common.R;
import com.pscxtc.domain.Stock;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Optional;

/**
 * Shop 通用
 *
 * @author chenxu
 * @since 2021年04月02日
 */
public interface IStockCommon {

    @RequestMapping(value = "/updateStock", method= RequestMethod.GET)
    public R updateStock(@RequestParam("goodsId") String goodsId, @RequestParam("goodsNum") Long goodsNum);

    @RequestMapping(value = "/findOne", method = RequestMethod.POST)
    public Optional<Stock> findOne(@RequestBody Stock stock);
}
