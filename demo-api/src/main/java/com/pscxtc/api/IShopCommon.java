package com.pscxtc.api;

import com.pscxtc.common.R;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Shop 通用
 *
 * @author chenxu
 * @since 2021年04月02日
 */
public interface IShopCommon {

    @RequestMapping(value = "/create",method = RequestMethod.GET)
    public R create(@RequestParam("userId") String userId,@RequestParam("goodsId") String goodsId,@RequestParam("goodsCount") Long goodsCount);
}
