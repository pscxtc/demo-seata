package com.pscxtc.exception;

/**
 * 自定义异常
 *
 * @author chenxu
 * @since 2021年04月07日
 */
public class BusinessException extends RuntimeException{

    public BusinessException (String message){
        super(message);
    }

}
