package com.pscxtc.exception;

import com.pscxtc.common.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * TODO: 类注释
 *
 * @author chenxu
 * @since 2021年04月07日
 */
@ControllerAdvice
@ResponseBody
@Slf4j
//@ConditionalOnExpression("${exceptionFilter.enable:false}")
public class ExceptionFilter {

    @ExceptionHandler(value = BusinessException.class)
    public R businessExceptionHandler(BusinessException e){
        //绑定异常是需要明确提示给用户的
        log.warn(" BusinessException  :{} ",e.getMessage());
        // 其余异常简单返回为服务器异常
        return R.error(e.getMessage());
    }

    @ExceptionHandler(value = Exception.class)
    public R exceptionHandler( Exception e){
        //绑定异常是需要明确提示给用户的
        log.warn(" Exception  :{} ",e.getMessage(),e);
        // 其余异常简单返回为服务器异常
        return R.error("网络异常,请联系管理员");
    }

    @ExceptionHandler(value = RuntimeException.class)
    public R runtimeExceptionHandler( RuntimeException e){
        //绑定异常是需要明确提示给用户的
        log.warn(" RuntimeException  :{} ",e.getMessage(),e);
        // 其余异常简单返回为服务器异常
        return R.error("系统繁忙,请稍后重试");
    }
}
