package com.pscxtc.snowflake;

import cn.hutool.core.lang.Snowflake;

/**
 * TODO: 类注释
 *
 * @author chenxu
 * @since 2021年04月06日
 */
public class IdUtil {

    public static final Snowflake SNOW_FLAKE = new Snowflake(1L,1L);

    /**
     * 创建 id
     * @param
     * @return {@link String}
     * @since 2021/4/6
     * 版本历史:
     * Date         Author         Description
     *---------------------------------------------------------*
     * 2021/4/6   chenxu          初始创建
     */
    public static String createId(){
        return SNOW_FLAKE.nextIdStr();
    }
}
